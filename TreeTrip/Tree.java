package TreeTrip;

import java.util.ArrayList;
import java.util.List;

class Tree {

    private Node root;

    public void add(Node node) {
        if (root == null) {
            root = node;
            System.out.println("Root`s been setted");
        } else {
            Node current = root;
            Node parent;
            while (true) {
                parent = root;
                if (node.getValue() < current.getValue()) {
                    current = current.getLeft();
                    if (current == null) {
                        parent.setLeft(node);
                        System.out.println("Left`s been setted");
                        return;
                    }
                } else {
                    current = current.getRight();
                    if (current == null) {
                        parent.setRight(node);
                        System.out.println("Right`s been setted");
                        return;
                    }
                }
            }
        }
    }

    public boolean findNode(Node node) {
        boolean flag = false;
        if (root == null) {
            return false;
        } else {
            Node current = root;
            if (current.getValue() == node.getValue()) {
                flag = true;
                return flag;
            }
            if (flag == false && current.getLeft() != null) {
                findNode(current.getLeft());
            }
            if (flag == false && current.getRight() != null) {
                findNode(current.getRight());
            }

        }
        return flag;
    }

}

